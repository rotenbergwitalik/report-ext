const table = document.getElementsByTagName('table')[0];
const countOfTheDays = table.getElementsByTagName('tr').length;

const times = document.querySelectorAll('h3.pull-right')[0].children;

let strs = [];
for(time of times) {
    strs.push(time.textContent.split(":").map((item) => parseInt(item)))
}

const dirtyMins = strs[0][1] + strs[1][1];
const hours = (strs[0][0] + strs[1][0]) + parseInt(dirtyMins / 60);
const minutes = dirtyMins % 60;

const allTime = `${hours}:${minutes}`;


const minutesOnDay = (hours * 60 + minutes) / countOfTheDays;
const hoursOnDay = parseInt(minutesOnDay / 60);
const middleTimeOfDay = `${hoursOnDay}:${parseInt((minutesOnDay / 60 - hoursOnDay) * 60)}`

console.log(middleTimeOfDay, 'Middle time of the day')
console.log(allTime, 'ALL Time');

const createInfoDiv = (text, data) => {
    let elem = document.createElement("div");
    elem.appendChild(document.createTextNode(`${text}: ${data}`));
    return document.createElement("div").appendChild(elem);
}

const countOfTheDaysNode = createInfoDiv("Количество отработаных дней в этом месяце", countOfTheDays);
const allTimeNode = createInfoDiv("Общее время отработанное за этот месяц", allTime);
const needTimeOnCurrentDayNode = createInfoDiv("Должно быть отработано часов на текущий день", 8 * countOfTheDays);
const middleTimeOfDayNode = createInfoDiv("Среднее Количество часов в день", middleTimeOfDay);


const header = document.getElementsByClassName('container')[0];
let injectableData = document.createElement('div');
injectableData.classList.add("container");
injectableData.style.width = '90%';
injectableData.appendChild(countOfTheDaysNode);
injectableData.appendChild(allTimeNode);
injectableData.appendChild(needTimeOnCurrentDayNode);
injectableData.appendChild(middleTimeOfDayNode);
header.parentNode.insertBefore(injectableData, header.nextSibling);
